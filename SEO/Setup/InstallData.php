<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 09/01/2019
 * Time: 03:41
 */

namespace Doanh\SEO\Setup;


use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $resourceConfig;

    public function __construct(Config $resourceConfig)
    {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        $setup->startSetup();
        $this->resourceConfig->saveConfig('catalog/seo/category_canonical_tag', true, \Magento\Config\Block\System\Config\Form::SCOPE_DEFAULT, 0);

        $this->resourceConfig->saveConfig('catalog/seo/product_canonical_tag',
            true,
            \Magento\Config\Block\System\Config\Form::SCOPE_DEFAULT,
            0);
    }
}