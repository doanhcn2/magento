<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 09/01/2019
 * Time: 04:34
 */

namespace Doanh\HelloWorld\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Subscription extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init('doanh_helloworld_subscription', 'subscription_id');
    }
}