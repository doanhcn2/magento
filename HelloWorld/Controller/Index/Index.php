<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 07/01/2019
 * Time: 14:57
 */
namespace Doanh\HelloWorld\Controller\Index;
//use ;
//use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
//use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action {
    /**	@var	\Magento\Framework\View\Result\PageFactory		*/
    protected $resultFactory;
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultFactory)
    {
        $this->resultFactory = $resultFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        // TODO: Implement execute() method.
        $resultPage = $this->resultFactory->create(
            \Magento\Framework\Controller\ResultFactory::TYPE_PAGE
        );
        return $resultPage;
    }
}