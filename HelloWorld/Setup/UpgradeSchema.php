<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 09/01/2019
 * Time: 04:36
 */

namespace Doanh\HelloWorld\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement upgrade() method.
        if (version_compare($context->getVersion(), '2.0.1') < 0) {
            $installer = $setup;

            $installer->startSetup();

            // install new database table

            $table = $installer->getConnection()->newTable(
                $installer->getTable('doanh_helloworld_subscription')
            )
                ->addColumn(
                'subscription_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Subscription ID'
                )->addColumn(
                    'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                    [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ],
                'Created at')
                ->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Updated at'
                )
                ->addColumn(
                    'firstname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    64,
                    ['nullable' => false],
                    'First name'
                )
                ->addColumn(
                    'lastname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    64,
                    ['nullable' => false],
                    'Last name'
                )
                ->addColumn(
                    'email',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Email address'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,[
                    'nullable' => false,
                    'default' => 'pending',
                ],
                    'Status'
                )
                ->addColumn(
                    'message',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '64k',[
                    'unsigned' => true,
                    'nullable' => false
                ],
                    'Subscription notes'
                )
                ->addIndex(
                    $installer->getIdxName('doanh_helloworld_subscription', ['email']),
                    ['email']
                )->setComment('Cron schedule');

            $installer->getConnection()->createTable($table);

            $installer->endSetup();
        }
    }
}