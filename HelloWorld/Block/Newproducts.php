<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 08/01/2019
 * Time: 14:49
 */

namespace Doanh\HelloWorld\Block;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Newproducts extends Template
{
    private $_productCollectionFactory;

    public function __construct(Template\Context $context, CollectionFactory $productCollectionFactory, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function getProducts(){
        $collection = $this->_productCollectionFactory->create();

        $collection->addAttributeToSelect('*')->setOrder('created_at')->setPageSize(5);
        return $collection;
    }
}