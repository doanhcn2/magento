<?php
/**
 * Created by PhpStorm.
 * User: doanhcn2
 * Date: 08/01/2019
 * Time: 13:37
 */
namespace Doanh\HelloWorld\Block;
use Magento\Framework\View\Element\Template;

class Landingspage extends Template {
    public function getLandingsUrl(){
        return $this->getUrl('helloworld');
    }

    public function getRedirectUrl(){
        return $this->getUrl('helloworld/index/redirect');
    }
}